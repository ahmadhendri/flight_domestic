<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="font-awesome.min.css">
    <link rel="stylesheet" href="style.css">
    <title>flight domestic</title>
</head>
<body>

<div id="bigBox">
    <div class="pb">
        <h2><i class="fa fa-file-o"></i> Booking Information</h2>
        <h3><i class="fa fa-arrow-circle-o-right"></i> Global Information</h3>
        <table class="tab-info-xsp">
            <tr>
                <td>Booking Code</td>
                <td><i class="fa fa-angle-right"></i></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="padbox2">
        <h2>Flight Data <i class="fa fa-trash"></i></h2>
    </div>    
</div>

</body>
</html>